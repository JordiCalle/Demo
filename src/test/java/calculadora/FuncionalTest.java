package pruebas;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FuncionalTest {

	public static void main(String[] args) {
		WebDriver driver;
    	//System.setProperty("webdriver.firefox.marionette", "C:\\geckodriver.exe");
    	System.setProperty("webdriver.chrome.driver", "/usr/lib/chromium/chromedriver");
    	
		//driver = new ChromeDriver();
		driver = new PhantomJSDriver();
		
        String url = "https://images.google.com";
        String expectedTitle = "Google Images";
        String actualTitle = "";

        // Iniciar el navegador en la URL indicada
        driver.get(url);

        // Capturar el t�tulo del sitio
        actualTitle = driver.getTitle();

        // Comparar t�tulo de la p�gina con el esperado
        if (actualTitle.contentEquals(expectedTitle)){
            System.out.println("�T�tulo correcto!");
        } else {
            System.out.println("T�tulo incorrecto :(");
        }
       
        // Cerrar navegador
        driver.close();
	}
}